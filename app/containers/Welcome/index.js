/**
 *
 * Welcome
 *
 */

import React from 'react';
import { Text, View, FlatList, SafeAreaView, Button, Image, Linking, ScrollView, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { injectIntl, intlShape } from 'react-intl';

import LocaleToggle from 'containers/LanguageProvider/LocaleToggle';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectWelcome from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import images from 'images';
import { styles } from './styles';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

export function Welcome(props) {
  useInjectReducer({ key: 'welcome', reducer });
  useInjectSaga({ key: 'welcome', saga });
  const DATA = [
    {
      id: '1',
      name: 'Abdul',
      comments: 'First name',
    },
    {
      id: '2',
      name: 'Sathar',
      comments: 'Middle name',
    },
    {
      id: '3',
      name: 'Mohammed',
      comments: 'Last name',
    },
    {
      id: '4',
      name: 'Ali',
      comments: 'Last name1',
    },
  ];

  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware();
  // mount it on the Store
  const store = createStore(reducer, applyMiddleware(sagaMiddleware));

  const Item = ({ id, title, comments }) => (
    <View style={styles.item}>
      <Text style={styles.title}>{id}</Text>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.title}>{comments}</Text>
    </View>
  );

  const renderItem = ({ item }) => <Item id={item.id} comments={item.comments} title={item.name} />;

  return (
    <SafeAreaView>
      {/* <Image source={images.concept} resizeMode="cover" style={styles.bgImage} /> */}
      <ScrollView style={{ minHeight: Dimensions.get('window').height }}>
        <View style={styles.textWrapper}>
          {/* <Text style={styles.text}>{props.intl.formatMessage(messages.hello)}</Text> */}
          {/* <Image source={images.logo} resizeMode="contain" /> */}
          <Text style={styles.textBold}>Machine Test</Text>

          {/* <Text style={styles.text}>{props.intl.formatMessage(messages.explanation)}</Text> */}

          {/* <View style={styles.infoTextWrapper}>
            <Text styles={styles.text}>{props.intl.formatMessage(messages.goDocs)}</Text>
            <Button
              color="#12D4C1"
              title="--> Github"
              onPress={() => {
                Linking.openURL('https://github.com/keremcubuk/react-native-boilerplate');
              }}
            />
          </View> */}
          {/* <View style={styles.helpWrapper}>
            <Text styles={styles.text}>{props.intl.formatMessage(messages.readyToDev)} 🚀</Text>
            <Button color="#12D4C1" title="--> Go to Help Screen" onPress={() => props.navigation.navigate('Help')} />
          </View> */}
          <FlatList data={DATA} renderItem={renderItem} keyExtractor={item => item.id} />
          {/* <View style={styles.langWrapper}>
            <Text>{props.intl.formatMessage(messages.chooseLanguage)}</Text>
            <View style={styles.localeWrapper}>
              <LocaleToggle />
            </View>
          </View> */}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

Welcome.propTypes = {
  dispatch: PropTypes.func.isRequired,
  intl: intlShape,
};

const mapStateToProps = createStructuredSelector({
  welcome: makeSelectWelcome(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(injectIntl(Welcome));
